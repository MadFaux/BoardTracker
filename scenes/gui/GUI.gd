extends Container

var close = false;

func _input(ev):
	if (ev.is_action("escape")):
		$Pause.popup_centered();

func _on_Dice_roller_pressed():
	$"Tools/Tabs".current_tab = 0;

func _on_Encounters_pressed():
	$"Tools/Tabs".current_tab = 1;

func _on_GM_Tools_pressed():
	$"Tools/Tabs".current_tab = 2;

func _on_FileImage_file_selected(path):
	var image = load(path);
	if ($QuickPanel/Inspector.selection != null && image != null):
		$QuickPanel/Inspector.selection.image = image;
		$QuickPanel/Inspector/Icon.texture = image;
		$QuickPanel/Inspector.selection.update_img = true;

func _on_OpenClose_pressed():
	if (close == false):
		close = true;
		$OpenClose.text = "Tools";
		$Tools/CloseOpen.play("CloseOpen");
	else:
		close = false;
		$Tools.visible = true;
		$OpenClose.text = "Close";
		$Tools/CloseOpen.play_backwards("CloseOpen");
extends PopupPanel

func _on_Resume_pressed():
	hide();

func _on_Exit_pressed():
	$"/root/Tracker".goto_scene("res://scenes/menu_screens/Main menu.tscn");

func _on_Quit_pressed():
	get_tree().quit();

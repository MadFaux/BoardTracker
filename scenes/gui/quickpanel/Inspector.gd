extends VBoxContainer

var selection = null;

func _ready():
	set_process(true);

func _process(delta):
	if (get_node("/root/Tracker").Cam != null):
		var cam = get_node("/root/Tracker").Cam;
		if (cam.selection.size() != 1 && selection != null):
			visible = false;
			selection = null;
			$"Show Name".pressed = false;
			$Light/Light.pressed = false;
			$Light/Bright.text = " ft";
			$Light/Dim.text = " ft";
			$Icon.texture = null;
			$Name/Name.text = "";
			$Name/Name.editable = false;
		elif (cam.selection.size() == 1 && selection == null):
			visible = true;
			selection = cam.selection[0];
			$"Show Name".pressed = selection.show_name;
			$Light/Light.pressed = selection.light;
			$Light/Bright.text = var2str(selection.bright) + " ft";
			$Light/Dim.text = var2str(selection.dim) + " ft";
			$Icon.texture = selection.image;
			$Name/Name.text = selection.tag;
			$Name/Name.editable = true;
		elif (selection != null):
			selection.tag = $Name/Name.text;
		if (cam.selection.size() == 1 && selection != cam.selection[0]):
			visible = true;
			selection = cam.selection[0];
			$"Show Name".pressed = selection.show_name;
			$Light/Light.pressed = selection.light;
			$Light/Bright.text = var2str(selection.bright) + " ft";
			$Light/Dim.text = var2str(selection.dim) + " ft";
			$Icon.texture = selection.image;
			$Name/Name.text = selection.tag;
			$Name/Name.editable = true;

func _on_Remove_pressed():
	if (selection != null):
		selection.queue_free();
		# Disable the inspector again
		visible = false;
		selection = null;
		$"Show Name".pressed = false;
		$Light/Light.pressed = false;
		$Light/Bright.text = " ft";
		$Light/Dim.text = " ft";
		$Icon.texture = null;
		$Name/Name.text = "";
		$Name/Name.editable = false;
		if (get_node("/root/Tracker").Cam != null):
			get_node("/root/Tracker").Cam.selection = [];

func _on_Icon_gui_input(ev):
	if (ev.is_pressed() && ev.is_action("drag")):
		$"../../FileImage".popup_centered();

func _on_Show_Name_toggled(button_pressed):
	selection.show_name = button_pressed;

func _on_Light_toggled(button_pressed):
	selection.light = button_pressed;

func _on_Save_pressed():
	$"../../FileLoadSave".window_title = "Save a token";
	$"../../FileLoadSave".filters = ["*.token; Predefined token"];
	$"../../FileLoadSave".mode = $"../../FileLoadSave".MODE_SAVE_FILE;
	$"../../FileLoadSave".popup_centered();

func _on_Duplicate_pressed():
	pass

func _on_Bright_text_entered(new_text):
	selection.bright = new_text.to_int();
	$Light/Bright.text = var2str(new_text.to_int()) + " ft";

func _on_Dim_text_entered(new_text):
	selection.dim = new_text.to_int();
	$Light/Dim.text = var2str(new_text.to_int()) + " ft";

func _on_FileLoadSave_file_selected(path):
	if ($"../../FileLoadSave".mode == $"../../FileLoadSave".MODE_SAVE_FILE && selection != null):
		var save_token = File.new();
		save_token.open(path, File.WRITE);
		
		var token_data = selection.save();
		save_token.store_line(to_json(token_data));
		
		print("Successfully saved token to file: " + path);
		save_token.close();
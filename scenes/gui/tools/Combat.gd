extends Tabs

var num_combatants = 0;
var turn = 0;
var action = 0;
var initiatives = {};
var sorted = false;

class CombatSorter:
	static func sort(a, b):
		if a.initative > b.initative:
			return true;
		return false;

func _on_Token_pressed():
	var inspector = $"../../../QuickPanel/Inspector";
	if (inspector.selection != null):
		var combatant = preload("res://scenes/gui/tools/combatant/Combatant.tscn").instance();
		combatant.token = inspector.selection;
		$Scroll/Combatants.add_child(combatant);
		num_combatants += 1;

func _on_NonToken_pressed():
	var combatant = preload("res://scenes/gui/tools/combatant/Combatant.tscn").instance();
	$Scroll/Combatants.add_child(combatant);
	num_combatants += 1;
 
func _on_Next_pressed():
	_sort_combatants();
	if (num_combatants > 0):
		turn += 1;
		if (turn > num_combatants):
			turn = 1;
		if (turn == 1):
			$Scroll/Combatants.get_child($Scroll/Combatants.get_children().size() - 1).turn = false;
		else:
			$Scroll/Combatants.get_child(turn - 1).turn = false;
		$Scroll/Combatants.get_child(turn).turn = true;

func _on_Prev_pressed():
	_sort_combatants();
	if (num_combatants > 0):
		turn -= 1;
		if (turn < 1):
			turn = $Scroll/Combatants.get_children().size() - 1;
			$Scroll/Combatants.get_child(1).turn = false;
		else:
			$Scroll/Combatants.get_child(turn + 1).turn = false;
		$Scroll/Combatants.get_child(turn).turn = true;

func _sort_combatants():
	if (num_combatants > 1 && !sorted):
		var combatants = $Scroll/Combatants.get_children();
		combatants.pop_front();
		combatants.sort_custom(CombatSorter, "sort");
		var i = 1;
		for c in combatants:
			$Scroll/Combatants.move_child(c, i);
			i += 1;

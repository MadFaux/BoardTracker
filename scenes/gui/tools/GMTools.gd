extends Tabs

func _on_New_pressed():
	var token = preload("res://scenes/token/Token.tscn").instance();
	var root = get_node("/root/Root/Board");
	token.show_name = true;
	root.add_child(token);

func _on_Character_pressed():
	pass

func _on_NPC_pressed():
	pass

func _on_Monster_pressed():
	var token = preload("res://scenes/token/Token.tscn").instance();
	token.tag = "Goblin"
	token.light = false;
	var root = get_node("/root/Root/Board");
	root.add_child(token);

func _on_Wall_toggled(button_pressed):
	var cam = get_node("/root/Tracker").Cam;
	cam.draw_solid = button_pressed;

func _on_Fog_of_War_toggled(button_pressed):
	if (button_pressed):
		get_node("/root/Root/Board").material = preload("res://shaders/Light.tres");
		get_node("/root/Root/Camera/Mat").material = preload("res://shaders/Light.tres");
		get_node("/root/Tracker").Fog = true;
	else:
		get_node("/root/Root/Board").material = null;
		get_node("/root/Root/Camera/Mat").material = null;
		get_node("/root/Tracker").Fog = false;

func _on_Save_Game_pressed():
	get_node("/root/Tracker").save_game("Test.board");

func _on_Load_Game_pressed():
	get_node("/root/Tracker").load_game("Test.board");
	var insp = $"../../../Background/Inspector";
	if (insp.selection != null):
		# Disable the inspector again
		insp.visible = false;
		insp.selection = null;
		insp.get_node("Show Name").pressed = false;
		insp.get_node("Light/Light").pressed = false;
		insp.get_node("Icon").texture = null;
		insp.get_node("Name/Name").text = "";
		insp.get_node("Name/Name").editable = false;
		if (get_node("/root/Tracker").Cam != null):
			get_node("/root/Tracker").Cam.selection = [];

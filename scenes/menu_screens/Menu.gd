extends VBoxContainer

func _on_Host_pressed():
	$"/root/Tracker".Host = true;
	get_node("/root/Tracker").goto_scene("res://scenes/menu_screens/lobby/Lobby.tscn");

func _on_Join_pressed():
	$"/root/Tracker".Host = false;
	get_node("/root/Tracker").goto_scene("res://scenes/menu_screens/lobby/Lobby.tscn");

func _on_Offline_pressed():
	get_tree().change_scene("res://scenes/table/Table.tscn");

func _on_Settings_pressed():
	visible = false;
	$"../Settings".visible = true;

func _on_Quit_pressed():
	get_tree().quit();
	
extends VBoxContainer

func _ready():
	$Resolution.add_item("1280x720");
	$Resolution.add_item("1920x1080");
	$Resolution.add_item("1920x1200");
	$Resolution.add_item("2560x1440");
	$Resolution.add_item("3840x2160");
	$Resolution.add_item("4096x2160");

func _on_Fullscreen_toggled(button_pressed):
	OS.window_fullscreen = button_pressed;
	if (button_pressed):
		$Resolution.disabled = false;
	else:
		$Resolution.disabled = true;

func _on_Resolution_item_selected(ID):
	if (ID == 0):
		OS.window_size = Vector2(1280, 720);
	elif (ID == 1):
		OS.window_size = Vector2(1920, 1080);
	elif (ID == 2):
		OS.window_size = Vector2(1920, 1200);

func _on_Input_pressed():
	pass # replace with function body

func _on_Profile_pressed():
	pass # replace with function body

func _on_Revert_pressed():
	OS.window_fullscreen = false;
	$Fullscreen.pressed = false;
	$Resolution.disabled = true;

func _on_Back_pressed():
	visible = false;
	$"../Menu".visible = true;
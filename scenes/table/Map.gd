extends TileMap

func save():
	get_used_cells()
	var save_dict = {
		"scene" : get_filename(),
		"tiles" : get_used_cells(),
	}
	return save_dict

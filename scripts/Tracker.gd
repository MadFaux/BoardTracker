extends Node

var Cam = null;
var Fog = false;
var Host = false;
var Server = null;
var Port = 25565;
var IP = "127.0.0.1";
var MaxPlayers = 5;

func _ready():
	if (get_node("/root").has_node("Root")):
		if (get_node("/root/Root").has_node("Camera")):
			Cam = get_node("/root/Root/Camera");

func set_camera():
	if (get_node("/root").has_node("Root")):
		if (get_node("/root/Root").has_node("Camera")):
			Cam = get_node("/root/Root/Camera");

func multiplayer():
	if (Host):
		Server = NetworkedMultiplayerENet.new();
		Server.create_server(Port, MaxPlayers);
		get_tree().set_network_peer(Server);
		print("Succes!");
	else:
		Server = NetworkedMultiplayerENet.new();
		Server.create_client(IP, Port);
		get_tree().set_network_peer(Server);

func save_game(filepath):
	var settings = {
		"scene" : "Settings",
		"Fog" : Fog,
	};
	var save_game = File.new();
	save_game.open("user://" + filepath, File.WRITE);
	
	save_game.store_line(to_json(settings));
	
	var save_nodes = get_tree().get_nodes_in_group("Persist");
	for i in save_nodes:
		var node_data = i.save();
		save_game.store_line(to_json(node_data));
	print("Successfully saved to file: " + filepath);
	save_game.close();

func load_game(filepath):
	var save_game = File.new();
	if not save_game.file_exists("user://" + filepath):
		print("Warning: could not find the saved file!");
		return
	
	# Revert the game-state
	Cam.draw_solid = false;
	get_node("/root/Root/Canvas/GUI/Tools/Tabs/GM Tools/List/Wall").pressed = false;
	var save_nodes = get_tree().get_nodes_in_group("Persist");
	for i in save_nodes:
		if (i.name != "Map"):
			i.queue_free();
	
	# Load the file line by line and process that dictionary to restore the object it represents
	save_game.open("user://" + filepath, File.READ);
	
	while not save_game.eof_reached():
		var current_line = parse_json(save_game.get_line());
		if (current_line == null):
			continue
		
		# Map
		if (current_line["scene"] == ""):
			var map = get_node("/root/Root/Board/Map");
			map.clear();
			map.tile_set = preload("res://tilesets/Default.tres");
			for c in current_line["tiles"]:
				var split = c.split(",");
				map.set_cell(split[0].to_int(), split[1].to_int(), 0);
			continue
		# Settings
		elif (current_line["scene"] == "Settings"):
			Fog = current_line["Fog"];
			if (Fog):
				get_node("/root/Root/Board").material = preload("res://shaders/Light.tres");
				get_node("/root/Root/Camera/Mat").material = preload("res://shaders/Light.tres");
				get_node("/root/Root/Canvas/GUI/Tools/Tabs/GM Tools/List/Fog of War").pressed = true;
			else:
				get_node("/root/Root/Board").material = null;
				get_node("/root/Root/Camera/Mat").material = null;
				get_node("/root/Root/Canvas/GUI/Tools/Tabs/GM Tools/List/Fog of War").pressed = false;
			continue
		
		# Tokens
		var new = load(current_line["scene"]).instance();
		get_node(current_line["parent"]).add_child(new);
		new.position = Vector2(current_line["pos_x"], current_line["pos_y"]);
		# Now we set the remaining variables.
		for i in current_line.keys():
			if (i == "parent" || i == "pos_x" || i == "pos_y"):
				continue
			new.set(i, current_line[i]);
	
	print("Successfully loaded from file: " + filepath);
	save_game.close();

func goto_scene(filepath):
	call_deferred("_goto_scene", filepath);

func _goto_scene(filepath):
	get_tree().change_scene(filepath);